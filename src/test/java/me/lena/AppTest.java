package me.lena;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class AppTest {

    @Test
    public void test() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("https://www.webtran.ru/translate/serbian/");

        WebElement inputField = driver.findElement(By.id("gtlsource"));
        inputField.click();
        inputField.sendKeys("Здравствуй, Дедушка Мороз!");

        driver.findElement(By.id("gtltranslate")).click();

        WebElement outputField = driver.findElement(By.id("gtlresults_body"));
        outputField.sendKeys("Здраво Дедусхка Мороз!");

        outputField.click();
        String translated = outputField.getAttribute("value");
        Assertions.assertEquals("Здраво Дедусхка Мороз!", translated);
    }

}
